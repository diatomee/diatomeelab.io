let stars = [];
let shootingStar = null;
let drawnStars = []; // Liste des étoiles dessinées avec la souris

function setup() {
    
  createCanvas(windowWidth, windowHeight);
  // Création des étoiles de fond
  for (let i = 0; i < 200; i++) {
    stars.push(new Star());
  }

}

function draw() {
  clear();
  
  // Dessiner les étoiles de fond
  for (let star of stars) {
    star.update();
    star.show();
  }

  // Dessiner les étoiles filantes
  if (shootingStar) {
    shootingStar.update();
    shootingStar.show();
    if (shootingStar.isOffScreen()) {
      shootingStar = null;
    }
  } else if (random(1) < 0.002) {
    shootingStar = new ShootingStar();
  }

  // Dessiner les étoiles créées avec la souris
  for (let drawnStar of drawnStars) {
    drawnStar.update();
    drawnStar.show();
  }
}

// Classe pour une étoile
class Star {
  constructor(x = random(width), y = random(height)) {
    this.x = x;
    this.y = y;
    this.size = random(1, 3); // Taille petite comme les étoiles de fond
    this.brightness = random(150, 255);
    this.speed = random(0.2, 1);
    this.twinkleOffset = random(TWO_PI);
  }
  
  update() {
    // Mouvement vers les bords comme les étoiles de fond
    this.x += (this.x - width / 2) * 0.001 * this.speed;
    this.y += (this.y - height / 2) * 0.001 * this.speed;

    if (this.x < 0 || this.x > width || this.y < 0 || this.y > height) {
      this.x = random(width);
      this.y = random(height);
    }
  }
  
  show() {
    let twinkle = sin(frameCount * 0.1 + this.twinkleOffset) * 50;
    let currentBrightness = constrain(this.brightness + twinkle, 150, 255);

    fill(currentBrightness, currentBrightness, 255, 200);
    noStroke();
    
    ellipse(this.x, this.y, this.size + map(currentBrightness, 150, 255, 0, 2));
  }
}

// Classe pour une étoile filante
class ShootingStar {
  constructor() {
    this.x = random(width);
    this.y = random(height / 2);
    this.len = random(50, 150);
    this.speed = random(5, 10);
  }
  
  update() {
    this.x += this.speed;
    this.y += this.speed / 2;
  }
  
  show() {
    stroke(255);
    strokeWeight(2);
    line(this.x, this.y, this.x - this.len, this.y - this.len / 2);
  }
  
  isOffScreen() {
    return this.x > width || this.y > height;
  }
}

function mouseMoved() {
  // Ajouter une nouvelle étoile à la position de la souris lorsqu'elle bouge
  drawnStars.push(new Star(mouseX, mouseY));
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
