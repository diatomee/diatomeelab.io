# Portail de mes projets (Diatomée)

Ce dépôt contient un petit site statique minimaliste qui liste et détaille tous les projets pertinents tenus sur mon compte Gitlab. L’adresse du site est https://diatomee.gitlab.io/.